<?php
$saveUser = no;
$nombre = $_POST['name'];
$email = $_POST['email'];
$clave = $_POST['password'];
$ciudad = $_POST['city'];
$saveUser = $_POST['saveUser'];
$vaNombre = '';
$valCiudad = '';
$valEmail = '';
if($saveUser == 'yes'){
	if(!empty($email)){
		if (! email_exists( $email ) ) {
			$email = strtolower($email);
			$user_id = wp_create_user( $email, $clave, $email );
			wp_update_user(
				array(
					'ID'          =>    $user_id,
					'nickname'    =>    $email,
					'first_name'  =>	$nombre,
					'show_admin_bar_front' => false
				)
			);
			$user = new WP_User( $user_id );
			$user->set_role( 'subscriber' );
			add_user_meta( $user_id, '_active', 'yes');
			add_user_meta( $user_id, 'city',$ciudad );
			add_user_meta( $user_id, '',$ciudad );
			$subject = "Bienvenido a IVC EN VIVO";
			$message = "<strong>Felicitaciones, Tu cuenta de IVC EN VIVO ya se encuentra activa!</strong>
			<p>Puedes ingresar <a href='<?php get_bloginfo('url'));?>' target='_blank'>ivcenvivo.com</a> con las siguientes credenciales:</p>
			<p><strong>usuario:</strong> ".$email."<br />
			<strong>contraseña:</strong> ".$clave."</p>
			<p>Atentamente,<br />Equipo IVC</p>";
			$headers = array('Content-Type: text/html; charset=UTF-8','From: IVC <info@ivcenvivo.com>');
			$result = wp_mail( $email, $subject, $message, $headers );
			wp_set_current_user($user_id);
			wp_set_auth_cookie($user_id);
			$render = true;
		}else{
			$error_msg = 'Ya existe un usuario registrado con este email';
		}
	}else{
		$error_msg = 'Completa todos los campos obligatorios *';
	}
} 
get_header(); 
?>
<div id="content" class="main-area">
	<?php if ( is_user_logged_in() ) { 
	if($saveUser=='yes'){ ?>
	<div id="wrap-content">
		<div class="success_form">
			<p>El registro se completó de forma exitosa</p>
			<a class="btn_live" href="<?php echo get_bloginfo('url');?>/live/">Ir a la señal en vivo</a>
		</div>
	</div>
	<?php } ?>
	<?php }else{ ?>
	<div id="wrap-content" class="init">
		<div class="col_home" id="new_user">
			<div class="block_content">
				<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam no</p>
				<strong>SUSCRIBIRSE</strong>
				<?php if(!empty($error_msg)){
					echo '<div class="alert_msg">'.$error_msg.'</div>';
					$valNombre = 'value="'.$nombre.'" class="error"';
					$valCiudad = 'value="'.$ciudad.'" class="error"';
					$valEmail = 'value="'.$email.'" class="error"';
				}?>
				<form method="POST" action="">
					<div class="item_form_ss">
						<input type="text" name="name" placeholder="NOMBRE" required <?php echo $valNombre; ?>>
					</div>
					<div class="item_form_ss">
						<input type="email" name="email" placeholder="EMAIL" required <?php echo $valEmail; ?>>
					</div>
					<div class="item_form_ss">
						<input type="text" name="city" placeholder="CIUDAD" required <?php echo $valCiudad; ?>>
					</div>
					<div class="item_form_ss">
						<select name="gender" id="gener" required>
						  <option value="Femenino">FEMENINO</option>
						  <option value="Masculino">MASCULINO</option>
						</select>
					</div>
					<div class="item_form_ss">
						<input type="password" name="password" id="password" placeholder="CONTRASEÑA" required>
					</div>
					<div class="item_form_ss">
						<input type="password" name="password2" id="confirmPassword" placeholder="CONFIRMAR CONTRASEÑA" required>
						<label class="msg-pass" style="display: none;"></label>
					</div>
					<div class="item_form_ss">
						<input type="hidden" id="saveUser" name="saveUser" value="yes">
						<input type="submit" name="registrarse" value="CONTINUAR">
					</div>
				</form>
				<ul class="social-btn">
					<li><a href="https://www.facebook.com/Ivcnet-1733388843548934/?ref=br_rs" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
					<li><a href="https://twitter.com/ivcnetworks" target="_blank"><i class="fab fa-twitter"></i></a></li>
					
					<li><a href="https://www.instagram.com/ivcnetworks/" target="_blank"><i class="fab fa-instagram"></i></a></li>
					<li><a href="https://www.youtube.com/channel/UCz22gaAIPdIfkYOSXYG6bqQ" target="_blank"><i class="fab fa-youtube"></i></a></li>
				</ul><!-- social-btn -->
			</div>
		</div>
		<div class="col_home">
			<div class="block_content">
				<img src="<?php echo get_template_directory_uri();?>/img/logo_ivc.png" />
				<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam no</p>
				<strong>INICIAR SESIÓN</strong>
				<?php echo do_shortcode('[wp_login_form label_username="EMAIL" label_password="CONTRASEÑA" label_log_in="INGRESAR" remember=0 lost_password="0" redirect="'.get_bloginfo('url').'/live/"]');?>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		jQuery(function($){
	    	var mssc = $('.msg-pass');
	    	function validatepss(){
	    		var pss = $( "#password" ).val();
				var conf = $( "#confirmPassword" ).val();
				  mssc.fadeIn('fast');
				  if(pss === conf){
				  	mssc.removeClass('wrong-msg');
				  	mssc.addClass('correct-msg');
				  	mssc.text('Las contraseñas son correctas!');
				  }else{
				  	mssc.addClass('wrong-msg');
				  	mssc.removeClass('correct-msg');
				  	mssc.text('Las contraseñas no coinciden');
				  }
	        	}
	    	$( "#password, #confirmPassword" ).keyup(function() {
			  validatepss();
			});
		});
    </script>
	<?php } ?>
</div>
<?php get_footer(); ?>