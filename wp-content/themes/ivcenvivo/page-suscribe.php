<?php
/*Template Name: Page suscribe
*/
$nombre = $_POST['name'];
$email = $_POST['email'];
$clave = $_POST['password'];
$ciudad = $_POST['city'];
if($saveUser == 'yes'){
	if(!empty($email)){
		if (! email_exists( $email ) ) {
			$email = strtolower($email);
			$user_id = wp_create_user( $email, $clave, $email );
			wp_update_user(
				array(
					'ID'          =>    $user_id,
					'nickname'    =>    $email,
					'first_name'  =>	$nombre,
					'show_admin_bar_front' => false
				)
			);
			$user = new WP_User( $user_id );
			$user->set_role( 'subscriber' );
			add_user_meta( $user_id, '_active', 'yes');
			add_user_meta( $user_id, 'city',$ciudad );
			add_user_meta( $user_id, '',$ciudad );
			$subject = "Bienvenido a IVC EN VIVO";
			$message = "<strong>Felicitaciones, Tu cuenta de IVC EN VIVO ya se encuentra activa!</strong>
			<p>Puedes ingresar <a href='<?php get_bloginfo('url'));?>' target='_blank'>ivcenvivo.com</a> con las siguientes credenciales:</p>
			<p><strong>usuario:</strong> ".$email."<br />
			<strong>contraseña:</strong> ".$clave."</p>
			<p>Atentamente,<br />Equipo IVC</p>";
			$headers = array('Content-Type: text/html; charset=UTF-8','From: IVC <info@ivcenvivo.com>');
			$result = wp_mail( $email, $subject, $message, $headers );
			wp_set_current_user($user_id);
			wp_set_auth_cookie($user_id);
			$render = true;
		}else{
			$error_msg = 'Ya existe un usuario registrado con este email';
		}
	}else{
		$error_msg = 'Completa todos los campos obligatorios *';
	}
}
get_header();
?>