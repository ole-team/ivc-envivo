<?php
/*Template Name: Page Live
*/
$nombre = $_POST['name'];
$email = $_POST['email'];
$clave = $_POST['password'];
$ciudad = $_POST['city'];
get_header(); 
?>
<div id="content" class="main-area">
	<?php while ( have_posts() ) : the_post();
		if(is_user_logged_in()){ ?>
	<div class="user-bar">
		<div class="menu-user"><span>OPCIONES</span>
			<div class="user-items">
				<div class="item_options">
					<a href="<?php echo get_bloginfo('url');?>/new-password/">CAMBIAR CONTRASEÑA</a>
				</div>
				<div class="item_options">
					<a href="<?php echo wp_logout_url(get_bloginfo('url')); ?>">CERRAR SESIÓN</a>
				</div>
			</div>
		</div>
	</div>
	<div id="wrap-content" class="inside">
		<img src="<?php echo get_template_directory_uri();?>/img/logo_ivc.png" class="logo" />
		<?php
		$idp = get_the_ID();
		$iframe = get_post_meta($idp,'live_embed',true);
		if($iframe){ ?>
		<div id="videoad_wrapp">
			<div class="video_wrapper_responsive">
			<?php echo $iframe; ?>
			</div>
		</div>
		<?php }?>

		<h1><?php echo get_the_title(); ?></h1>
		<ul class="social-btn">
				<li><a href="https://www.facebook.com/Ivcnet-1733388843548934/?ref=br_rs" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
				<li><a href="https://twitter.com/ivcnetworks" target="_blank"><i class="fab fa-twitter"></i></a></li>
				
				<li><a href="https://www.instagram.com/ivcnetworks/" target="_blank"><i class="fab fa-instagram"></i></a></li>
				<li><a href="https://www.youtube.com/channel/UCz22gaAIPdIfkYOSXYG6bqQ" target="_blank"><i class="fab fa-youtube"></i></a></li>
			</ul><!-- social-btn -->
	</div>	
	<?php } endwhile; ?>
</div>
<?php get_footer(); ?>