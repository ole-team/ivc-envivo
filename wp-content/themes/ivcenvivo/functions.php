<?php
function ivcenvivo_setup() {
	// Agregar al tema la funcionalidad del menu
	register_nav_menu( 'mainmenu', 'Menú principal' );

	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 624, 9999 ); // Unlimited height, soft crop
	$defaults = array(
 'height'      => 240,
 'width'       => 70,
 'flex-height' => true,
 'flex-width'  => true,
 'header-text' => array( 'site-title', 'site-description' ),
 );
 add_theme_support( 'custom-logo', $defaults );
}
add_action( 'after_setup_theme', 'ivcenvivo_setup' );

function ivcenvivo_scripts_styles() {
	global $wp_styles;
	//cargar las librerias del tema
	//wp_enqueue_script( 'materialize-min', get_template_directory_uri() . '/js/materialize.min.js', array('jquery'));
	/*wp_enqueue_script( 'carrusel', get_template_directory_uri() . '/js/jquery.carouFredSel-6.2.1.js', array('jquery'));
	wp_enqueue_script( 'selectb', get_template_directory_uri() . '/js/jquery.customSelect.js', array('jquery'));
	wp_enqueue_script( 'awards', get_template_directory_uri() . '/js/liteaccordion.jquery.js', array('jquery'));
	wp_enqueue_script( 'awards2', get_template_directory_uri() . '/js/jquery.tabify.js', array('jquery'));
	wp_enqueue_script( 'recaptcha', 'https://www.google.com/recaptcha/api.js');
	wp_enqueue_style( 'homeslider-style', get_template_directory_uri().'/css/nivo-slider.css' );
	wp_enqueue_style( 'themeslider-style', get_template_directory_uri().'/css/themes/default/default.css' );
	wp_enqueue_style( 'awards-style', get_template_directory_uri().'/css/liteaccordion.css' );*/
	/*wp_enqueue_style( 'materialize-style', get_template_directory_uri() .'/css/materialize.css' );
	wp_enqueue_style( 'custom-css', get_template_directory_uri() .'/css/style.css' );*/
	wp_enqueue_style( 'ivcenvivo-style', get_stylesheet_uri() );

}
add_action( 'wp_enqueue_scripts', 'ivcenvivo_scripts_styles' );

function ivcenvivo_title( $title, $sep ) {
	global $paged, $page;

	if ( is_feed() )
		return $title;

	// Add the site name.
	$title .= get_bloginfo( 'name' );

	// Add the site description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		$title = "$title $sep $site_description";

	// Add a page number if necessary.
	if ( $paged >= 2 || $page >= 2 )
		$title = "$title $sep " . sprintf( __( 'Page %s', 'twentytwelve' ), max( $paged, $page ) );

	return $title;
}
add_filter( 'wp_title', 'ivcenvivo_title', 10, 2 );
function ivcenvivo_widgets_init() {
	register_sidebar( array(
		'name' => __( 'SIDEBAR', 'ivcenvivo' ),
		'id' => 'sidebar',
		'description' => __( 'Sidebar', 'ivcenvivo' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
	register_sidebar( array(
		'name' => __( 'Footer Bar', 'ivcenvivo' ),
		'id' => 'footerbar',
		'description' => __( 'Footer bar', 'ivcenvivo' ),
		'before_widget' => '<div id="%1$s">',
		'after_widget' => '</div>',
		'before_title' => '',
		'after_title' => '',
	) );
}
add_action( 'widgets_init', 'ivcenvivo_widgets_init' );
add_action('after_setup_theme', 'remove_admin_bar');
 
function remove_admin_bar() {
if (!current_user_can('administrator') && !is_admin()) {
  show_admin_bar(false);
}
}
add_action( 'admin_post_nopriv_your_action_name', 'your_function_to_process_form' );
function your_function_to_process_form(){
// process your form here
	wp_redirect( '/live' );
}

?>